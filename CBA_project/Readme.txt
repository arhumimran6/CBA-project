*** This code covers every little point, from preparation of data to designing models and comparing them ***

I prefer, running this code, everytime with raw file(original csv file that you shared), because data preparation
includes removing of un-necessary information and this whole process of data preparation is sequential; It can mess
with the output of columns which were removed during data preparation.

Run with original file and remember to rename it to 'csv_file'

This code is developed on 'Pycharm community edition'

libraries to install: sklearn, pandas, matplotlib

this .rar file consists of two csv files and one python file:
1) csv_file.csv (updated file after every operation)
2) original copy (original file that you shared)
3) python file (python script) 
