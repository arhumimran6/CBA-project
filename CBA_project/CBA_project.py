import csv
import pandas as pd
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
import sklearn.metrics as metrics
import math as m
from sklearn.ensemble import RandomForestClassifier


data1 = pd.read_csv("csv_file.csv")

# defining a new variable/column named remaining_lease_yrs defined from remaining_lease and converting it into integer values,
# As the data in remaining_lease columns is of the type 'string', for example, '61 years 04 months' we will convert this information
# into single value of months i-e '61*12 + 4 = 736 months'
# We are doing this by extracting digits from string and then performing the operations

numbers = []    # list to store extracted digits
in_months = []  # list to store converted values
in_yrs = []     # list to store year values

if "remaining_lease" in data1.columns:
    # extracting digits from information provided in string
    for s in data1.remaining_lease:
        for word in s.split():
            if word.isdigit():
                numbers.append(int(word))

        if len(numbers) == 1:
            numbers.append(0)

        in_months.append(numbers[0] * 12 + numbers[1])
        numbers.clear()

    for value in in_months:
        in_yrs.append(int(value / 12))

    # adding a new column named 'remaining_lease_yrs' with datatype integer
    data1["remaining_lease_months"] = in_months
    data1["remaining_lease_yrs"] = in_yrs
    # implementing change to csv file
    data1.to_csv("csv_file.csv", index=False)

if "lease_commence_date" in data1.columns:
    # Removing lease_commence_date and remaining_lease from csv file
    data1.drop(['lease_commence_date', 'remaining_lease'], axis=1, inplace=True)
    # implementing change to csv file
    data1.to_csv("csv_file.csv", index=False)

if "block" and "street_name" in data1.columns:
    # combining block and street_name information to derive new variable 'block_street'
    data1["block_street"] = data1["block"].astype(str) + " " + data1["street_name"].astype(str)
    # implementing change to csv file
    data1.to_csv("csv_file.csv", index=False)

    # Removing block and street_name from csv file
    data1.drop(['block', 'street_name'], axis=1, inplace=True)
    # implementing change to csv file
    data1.to_csv("csv_file.csv", index=False)


# Highest and lowest transaction volumes in Month-Year
print("\n----Highest and lowest transaction volumes in Month-Year----")
month_count = data1["month"].value_counts()
month_count = dict(month_count)

# Highest transaction volume
max_trans_monthYear = max(month_count, key=month_count.get)
print("\nMonth-year having highest transaction volume is: " + max_trans_monthYear + ", with value = " + str(month_count[max_trans_monthYear]))

# lowest transaction volume
min_trans_monthYear = min(month_count, key=month_count.get)
print("\nMonth-year having lowest transaction volume is: " + min_trans_monthYear + ", with value = " + str(month_count[min_trans_monthYear]))


# Highest and lowest transaction volumes in towns
print("\n\n----Highest and lowest transaction volumes in towns----")
town_count = data1["town"].value_counts()
town_count = dict(town_count)

# Highest transaction volume
max_trans_town = max(town_count, key=town_count.get)
print("\nTown having highest transaction volume is: " + max_trans_town + ", with value = " + str(town_count[max_trans_town]))

# lowest transaction volume
min_trans_town = min(town_count, key=town_count.get)
print("\nTown having lowest transaction volume is: " + min_trans_town + ", with value = " + str(town_count[min_trans_town]))

print("\n")

# displaying top 5 resale prices in terms of flat_type, block_street, town, floor_area_sqm, storey_range, resale_price
print("\n----Top 5 resale prices in terms of flat_type, block_street, town, floor_area_sqm, storey_range, resale_price----\n")

newdf = pd.DataFrame(data1, columns=['flat_type', 'block_street', 'town', 'floor_area_sqm', 'storey_range', 'resale_price'])
# Descending Order
newdf.sort_values(by = ["resale_price"], inplace = True, ascending = False)
print(newdf[0:5])

# displaying bottom 5 resale prices in terms of flat_type, block_street, town, floor_area_sqm, storey_range, resale_price
print("\n----Bottom 5 resale prices in terms of flat_type, block_street, town, floor_area_sqm, storey_range, resale_price----\n")
# Ascending Order
newdf = pd.DataFrame(data1, columns=['flat_type', 'block_street', 'town', 'floor_area_sqm', 'storey_range', 'resale_price'])
newdf.sort_values(by = ["resale_price"], inplace = True)
print(newdf[0:5])

# Copying Data1 and saving Data2
data2 = data1

# Removing Flat_type '1 ROOM' and 'MULTI-GENERATION' also removing from categorical definition
data2 = data2[data2.flat_type != '1 ROOM']
data2 = data2[data2.flat_type != 'MULTI-GENERATION']
data2.to_csv("csv_file.csv", index=False)

# showing categorical levels of flat_type
print("Categorical levels of flat_type:\n")
print(data2.flat_type,"\n")

# listing number of cases in flat_type
print("Number of cases in flat_type:\n")
print(data2.flat_type.unique(),"\n")

# listing their occurences
print("Occurences of all cases in flat_type:\n")
print(data2["flat_type"].value_counts(),"\n")

# Removing block_street from data2
if "block_street" in data2.columns:
    data2.drop(["block_street"], axis=1, inplace=True)
    data2.to_csv("csv_file.csv")

# Creating a new variable storey
if "storey" not in data2.columns:
    data2["storey"] = data2["storey_range"]
    data2.to_csv("csv_file.csv",index=False)

# Removing Storey_range from data2
if "storey_range" in data2.columns:
    data2.drop(["storey_range"], axis=1, inplace=True)
    data2.to_csv("csv_file.csv", index=False)

# Creating categorical level 40 to 51 and rearraging/combining accordingly
# This will be done by replacing all 40 TO above and less than 51 storey_ranges with 40 to 51 and arranging accordingly
numbers.clear()
for value in data2.storey:
    for word in value.split():
        if word.isdigit():
            numbers.append(int(word))
    if numbers[0] >= 40 and numbers[1] <= 51:
        data2 = data2.replace(to_replace=[value], value="40 TO 51")
    numbers.clear()

data2.to_csv("csv_file.csv",index=False)

# showing categorical levels in storey
print("Categorical levels in storey:\n")
print(data2.storey,"\n")

# listing number of cases in storey
print("Number of cases in storey:\n")
print(data2.storey.unique(),"\n")

# listing their occurences
print("Occurence of all cases in storey:\n")
print(data2["storey"].value_counts(),"\n")

# Removing 2 ROOM, 'Premium Maisonette' and 'improved Maisonette'
data2 = data2[data2.flat_type != '2 ROOM']
data2 = data2[data2.flat_type != 'Premium Maisonette']
data2 = data2[data2.flat_type != 'Improved-Maisonette']
data2.to_csv("csv_file.csv",index=False)

# Categorical levels of flat_type
print("Categorical levels of flat_type:\n")
print(data2.flat_type,"\n")

# Number of cases of flat_type
print("Number of cases of flat_type:\n")
print(data2.flat_type.unique(),"\n")

# Their Occurences
print("Occurences of cases of flat_type:\n")
print(data2["flat_type"].value_counts(),"\n")

# Printing number of cases and columns of data2 after data preparation
print("Number of cases in data2: " + str(len(data2.axes[0])))
print("Number of columns in data2: " + str(len(data2.columns)))

# Reason for exexuting preparation:
# Data preparation is essential if data has to be operated on machine learning algorithms, because raw data contains
# attributes of multiple data types and machine learning algorithms at their core operate on numberic digits, in our
# case of we have done the same thing i-e removed extra attributes or columns and refined the data converted datatypes to natural datatypes

#----LINEAR REGRESSION STARTED----

print("\n----LINEAR REGRESSION----")
# Implementing train-test-split 70-30
# identifying the linear relationship between variables using correlation method
print(data2.corr(method = 'pearson'))
# as we can see values are positive so linear relationship exists between numerical variables
# i-e floor_area_sqm, resale_price, remaining_lease_months, remaining_lease_years

# displaying all relationships and selecting the most linear scattered graph for further clearance

# displaying scatter plot between floor_area_sqm amd resale_price
plt.scatter(data2["floor_area_sqm"], data2["resale_price"])
plt.title("Scatter plot b/w floor_area_sqm and Resale_price")
plt.show(block=False)
plt.pause(3)
plt.close()

# displaying scatter plot between remaining_lease_yrs amd resale_price
plt.scatter(data2["remaining_lease_yrs"], data2["resale_price"])
plt.title("Scatter plot b/w remaining_lease_yrs and Resale_price")
plt.show(block=False)
plt.pause(3)
plt.close()

print("As the scatter plot between 'floor_are_sqm' amd 'resale_prices' is more linear than the plot between 'remaining_lease_yrs' and 'resale_prices'")
print("So selecting the scatter b/w floor_area_sqm and resale_price")

# Applying train-test split 70-30
x = data2["floor_area_sqm"]
y = data2["resale_price"]

# Calculating test values and train values
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.3) # splitting in 70-30 percent ratio

# Displaying test values and train values
plt.scatter(x_train, y_train, label="Training data", color='r', alpha=.7)
plt.scatter(x_test, y_test, label="Testing data", color='g', alpha=.7)
plt.legend()
plt.title("Test Train Split")
plt.show(block=False)
plt.pause(3)
plt.close()

# Now performing linear regression
obj = LinearRegression() # creating and instance/object of class

# To train model pass the train values
obj.fit(x_train.values.reshape(-1,1), y_train.values)

# Now predicting the x_test values and comparing with the y_test and checking their accuracy
prediction = obj.predict(x_test.values.reshape(-1,1))
print("Predicted values of x_test: ", prediction)

print("Values of y_test are:\n")
print(y_test)

# plotting prediction line against actual test data
plt.plot(x_test, prediction, label = 'Linear Regression', color='b')

# plotting scatter plot between x_test and y_test
plt.scatter(x_test, y_test, label='x_test or actual_test data', color='g', alpha=0.7)
plt.legend()
plt.show(block=False)
plt.pause(4)
plt.close()

# Evaluating Metrics
mae = metrics.mean_absolute_error(x_test, y_test)
mse = metrics.mean_squared_error(x_test, y_test)
rmse = m.sqrt(mse) #mse**(0.5)
r2 = metrics.r2_score(y_test, prediction)

print("\nResults of sklearn.metrics:")
print("MAE:",mae)
print("MSE:", mse)
print("RMSE:", rmse)
print("R-Squared:", r2)

# checking accuracy of the model
print("\nAccuracy of the model is: ", obj.score(x_test.values.reshape(-1,1), y_test))
# storing score/accuracy of linear regression for comparison later
la = obj.score(x_test.values.reshape(-1,1), y_test)

#----LINEAR REGRESSION ENDED----

#----MULTIVARIATE ADAPTIVE REGRESSION STARTED----

print("\n----MULTIVARIATE ADAPTIVE REGRESSION----")
# Creating an instance of Linear Regression() class
clf = LinearRegression()
x = data2["floor_area_sqm"]
y = data2["resale_price"]

x_train,x_test,y_train,y_test=train_test_split(x,y,test_size=0.3)
clf.fit(x_train.values.reshape(-1,1), y_train.values)

# printing the array of coefficients
print("\nArray of coefficients: ",clf.coef_)

# printing intercept
print("\nIntercept: ",clf.intercept_)

# Predicting
y_pred = clf.predict(x_test.values.reshape(-1,1))
print("\nPredicted values are:\n", y_pred)

# Evaluating Metrics
mae = metrics.mean_absolute_error(x_test, y_test)
mse = metrics.mean_squared_error(x_test, y_test)
rmse = m.sqrt(mse) #mse**(0.5)
r2 = metrics.r2_score(y_test, y_pred)

print("\nResults of sklearn.metrics:")
print("MAE:",mae)
print("MSE:", mse)
print("RMSE:", rmse)
print("R-Squared:", r2)

print("\nAccuracy of the model is: ", clf.score(x_test.values.reshape(-1,1), y_test))
# storing score/accuracy of multivariate adaptive regression for comparison later
ma = clf.score(x_test.values.reshape(-1,1), y_test)

#----MULTIVARIATE ADAPTIVE REGRESSION ENDED----

#----RANDOM FOREST TREE----
# random forest will not give good results because its target value is continous and will not classify, in case of continous target value regression performs best
print("\n----RANDOM FOREST TREE----")

x = data2['floor_area_sqm']
model = RandomForestClassifier(n_estimators=40)
y = pd.factorize(data2['floor_area_sqm'])[0]
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.3)

model.fit(data2['floor_area_sqm'].values.reshape(-1,1), y)

# Predicting
y_pred = model.predict(x_test.values.reshape(-1,1))

# Evaluating metrics
mae = metrics.mean_absolute_error(x_test, y_test)
mse = metrics.mean_squared_error(x_test, y_test)
rmse = m.sqrt(mse) #mse**(0.5)
r2 = metrics.r2_score(y_pred, y_test)

print("\nResults of sklearn.metrics:")
print("MAE:",mae)
print("MSE:", mse)
print("RMSE:", rmse)
print("R-Squared:", r2)

print("\nAccuracy of the model is: ", metrics.accuracy_score(y_test, y_pred))
print("\n")
# storing score/accuracy for comparison later
Rf = metrics.accuracy_score(y_test, y_pred)

# Comparing scores/accuracy
if la > ma and la > Rf:
    print("Linear regression model performed best")
elif ma > la and ma > la:
    print("Multi variate regression model performed best")
else:
    print("Random forest model performed best")

print("\n------END------")